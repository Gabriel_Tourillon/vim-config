# Install vim config

```bash
git clone https://gitlab.com/Gabriel_Tourillon/vim-config.git
mv vim-config/.vim* .
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim


vim

#### Dans vim 

:PluginInstall

####

source .vimrc
```
